import java.util.*;

class Solution {
    public int[] movesToStamp(String stamp, String target) {
        char[] s = target.toCharArray();
        char[] t = stamp.toCharArray();
        List<Integer> result = new ArrayList<>();
        boolean[] visited = new boolean[s.length];
        int totalStamps = 0;
        
        while (totalStamps < s.length) {
            boolean stamped = false;
            for (int i = 0; i <= s.length - t.length; i++) {
                if (!visited[i] && canStamp(s, i, t)) {
                    totalStamps += stamp(s, i, t);
                    visited[i] = true;
                    result.add(i);
                    stamped = true;
                    if (totalStamps == s.length) break;
                }
            }
            if (!stamped) return new int[0];
        }
        
        Collections.reverse(result);
        int[] res = new int[result.size()];
        for (int i = 0; i < result.size(); i++) {
            res[i] = result.get(i);
        }
        return res;
    }
    
    private boolean canStamp(char[] s, int start, char[] t) {
        for (int i = 0; i < t.length; i++) {
            if (s[start + i] != '?' && s[start + i] != t[i]) {
                return false;
            }
        }
        return true;
    }
    
    private int stamp(char[] s, int start, char[] t) {
        int stamped = 0;
        for (int i = 0; i < t.length; i++) {
            if (s[start + i] != '?') {
                s[start + i] = '?';
                stamped++;
            }
        }
        return stamped;
    }
}
